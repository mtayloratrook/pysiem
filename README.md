# pysiem

## Topics
  ### Logging Standard
  * Application Logging
  * Django Logging
  ### Host Logging
  * CPU
  * Memory
  * Disk
  ### Basic Alerting
  * Alerting on threshold breaks
  ### Basic Attacks
  * Bruteforce SSH
  * DDoS
  * Application failed login
  ### Performance Monitoring
  * Application Metrics
  * Host Metrics
  * Stress Testing
  ### Anomaly Detection
  * Comparison between threshold alerts and ML alerts
  * Elastic ML Demonstration
  * Python ML Examples
    * PyTorch
      http://pytorch.org/tutorials/
    * Prophet
      https://github.com/facebook/prophet

## Setup
`sudo apt-get -y install python3-pip`
`sudo mkdir /opt/pysiem`
`sudo chown ubuntu:ubuntu /opt/pysiem`
`git clone https://mtayloratrook@bitbucket.org/mtayloratrook/pysiem.git`
`pip3 install virtualenv`
`virtualenv /opt/pysiem`
